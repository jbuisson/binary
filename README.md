# Binary

A small suit of tools and sample application to illustrates the medium series of article ["It’s all just 0 and 1"](https://itnext.io/its-all-just-0-and-1-understanding-binary-8530629e1d1f).

## Binary Front

A small angular application converting with steps decimal into Hexadecimal (base 16) and Binary (base 2).

```
cd src/binary.front
ng serve
```
