import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BinaryReaderComponent } from './binary-reader/binary-reader.component';
import { DecimalToBinaryComponent } from './decimal-to-binary/decimal-to-binary';
import { BinaryToDecimalComponent } from './binary-to-decimal/binary-to-decimal';

@NgModule({
  declarations: [
    AppComponent,
    BinaryReaderComponent,
    DecimalToBinaryComponent,
    BinaryToDecimalComponent,
  ],
  imports: [
    FormsModule,
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
