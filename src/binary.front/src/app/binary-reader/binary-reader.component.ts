import { Component } from '@angular/core';
import * as utf8 from 'utf8';

interface IBinaryRow {
  Position: string;
  Bytes: IByte[];
}

interface IByte {
  Value: number;
  Binary: string;
  CharCode: string;
  Hexadecimal: string;

  Over: boolean;
  Signature: boolean;
}

@Component({
  selector: 'binary-reader',
  templateUrl: './binary-reader.component.html',
  styleUrls: ['./binary-reader.component.less']
})
export class BinaryReaderComponent {

  public Utf8: string = '';
  public FileType: string = '';

  public Error: string = '';

  public Bytes: number[] = [];
  public CharCode: string = '';

  public BinaryRows: IBinaryRow[] = [];

  public FileChange(event: InputEvent): void {
    const target = event.target as HTMLInputElement;

    if (!target || !target.files || !target.files.length)
      return;

    const file = target.files[0];
    const reader = new FileReader();

    reader.onload = this.OnBufferRead.bind(this, reader, file);
    reader.readAsArrayBuffer(file);
  }

  public ByteLeave(byte: IByte): void {
    byte.Over = false;
  }

  public ByteEnter(byte: IByte): void {
    byte.Over = true;
  }

  private OnBufferRead(fileReader: FileReader, file: File): void {
    const view = new DataView(fileReader.result as ArrayBuffer);

    this.Utf8 = '';
    this.Error = '';
    this.FileType = '';

    this.Bytes = [];
    this.CharCode = '';
    this.BinaryRows = [{ Position: this.PadLeft('', 8), Bytes: [] }];

    for (let i = 0; i < view.byteLength; i++) {
      const byte = view.getUint8(i);

      this.Bytes.push(byte);
      this.CharCode += String.fromCharCode(byte);

      if (i > 0 && i % 16 === 0)
        this.BinaryRows.push({ Position: this.PadLeft(this.BinaryRows.length.toString(), 8), Bytes: [] });

      this.BinaryRows[this.BinaryRows.length - 1].Bytes.push({
        Value: byte,
        Binary: this.Binary(byte),
        CharCode: String.fromCharCode(byte),
        Hexadecimal: this.Hexadecimal(byte),
        Over: false,
        Signature: false,
      });

      if (i >= 1023) {
        this.Error = `The file is too big (${file.size} bytes), only a preview of the content is shown`;
        break;
      }
    }

    try {
      if (!this.DetectFileType(file)) {
        utf8.decode(this.CharCode);
        this.FileType = 'UTF-8 file (without header)';
      }
      this.FileType += ` - Size: ${file.size} bytes`;
    } catch (error) { }
  }

  public DetectFileType(file: File): boolean {
    const dic = [
      { Name: 'IBM Storyboard bitmap file', Extensions: ['pic'], Bytes: [0x00] },
      { Name: 'IBM Storyboard bitmap file', Extensions: ['mov'], Bytes: [0x00] },
      { Name: 'Windows Program Information File', Extensions: ['pif'], Bytes: [0x00] },
      { Name: 'Mac Stuffit Self-Extracting Archive', Extensions: ['sea'], Bytes: [0x00] },
      { Name: 'IRIS OCR data file', Extensions: ['ytr'], Bytes: [0x00] },
      { Name: 'Alliance for Open Media (AOMedia) Video 1 (AV1) Image File', Extensions: ['avif'], Bytes: [0x00, 0x00, 0x00] },
      { Name: 'High Efficiency Image Container (HEIC)', Extensions: ['heic'], Bytes: [0x00, 0x00, 0x00] },

      { Name: 'Bitmap', Extensions: ['bmp', 'dip'], Bytes: [0x42, 0x4D] },
      { Name: 'Generic JPEGimage file', Extensions: ['jpe', 'jpeg', 'jpg'], Bytes: [0xFF, 0xD8] },
      { Name: 'JPEG/JFIF graphics file', Extensions: ['jfif', 'jpe', 'jpeg', 'jpg'], Bytes: [0xFF, 0xD8, 0xFF, 0xE0] },
      { Name: 'Digital camera JPG using Exchangeable Image File Format (EXIF)', Extensions: ['jpg'], Bytes: [0xFF, 0xD8, 0xFF, 0xE1] },
      { Name: 'Still Picture Interchange File Format (SPIFF)', Extensions: ['jpg'], Bytes: [0xFF, 0xD8, 0xFF, 0xE8] },

      { Name: 'UTF-8 file', Extensions: [], Bytes: [0xEF, 0xBB, 0xBF] },
    ].sort((a, b) => b.Bytes.length - a.Bytes.length);

    const extension = /(?:\.([^.]+))?$/.exec(file.name)[1];

    for (let i = 0; i < dic.length; i++) {
      let matching = true;

      if (!!dic[i].Extensions.length && !dic[i].Extensions.find(ext => ext == extension))
        continue;

      for (let j = 0; j < dic[i].Bytes.length && this.BinaryRows[0].Bytes.length > dic[i].Bytes.length; j++) {
        if (this.BinaryRows[0].Bytes[j].Value !== dic[i].Bytes[j]) {
          matching = false;
          break;
        }
      }

      if (matching) {
        this.FileType = dic[i].Name;

        for (let j = 0; j < dic[i].Bytes.length; j++) {
          this.BinaryRows[0].Bytes[j].Signature = true;
        }

        return true;
      }
    }

    this.FileType = 'Unknown file';
    return false;
  }

  public Hexadecimal(byte: number): string {
    return this.PadLeft(byte.toString(16), 2);
  }

  public Binary(byte: number): string {
    return this.PadLeft(byte.toString(2), 8);
  }

  private PadLeft(value: string, length: number, char: string = '0'): string {
    while (value.length < length)
      value = `${char}${value}`;

    return value;
  }
}
