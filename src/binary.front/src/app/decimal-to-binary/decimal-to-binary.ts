import { Component, OnInit } from '@angular/core';

interface IDivisionStep {
  Value: Number;
  Remainder: Number;
}

@Component({
  selector: 'decimal-to-binary',
  templateUrl: './decimal-to-binary.html',
  styleUrls: ['./decimal-to-binary.less']
})
export class DecimalToBinaryComponent implements OnInit {

  public HexMap = {
    10: 'A',
    11: 'B',
    12: 'C',
    13: 'D',
    14: 'E',
    15: 'F',
  };

  public BinaryDivisionSteps: IDivisionStep[] = [];
  public HexDivisionSteps: IDivisionStep[] = [];

  public Decimal: number = 1337;

  public ngOnInit(): void {
    this.ConvertAllBase();
  }

  public ConvertAllBase() {
    if (this.IsValidDecimal() && this.Decimal > 9999)
      this.Decimal = 9999;

    this.BinaryDivisionSteps = this.Convert(this.BinaryDivisionSteps, 2);
    this.HexDivisionSteps = this.Convert(this.HexDivisionSteps, 16);
  }

  public IsValidDecimal(): boolean {
    return !isNaN(this.Decimal) && this.Decimal > 0;
  }

  public Convert(steps: IDivisionStep[], base: number): IDivisionStep[] {
    steps = [];

    if (!this.IsValidDecimal())
      return;

    var result = this.Decimal;

    do {
      var remainder = result % base;

      steps.push({ Value: result, Remainder: remainder });

      result = Math.floor(result / base);
    } while (result >= base)

    if (result > 0)
      steps.push({ Value: 0, Remainder: result });

    return steps;
  }
}
