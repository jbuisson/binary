import { Component, OnInit } from '@angular/core';

interface IConversionStep {
  Base: number;
  Value: number;
  Remainder: number;
}

@Component({
  selector: 'binary-to-decimal',
  templateUrl: './binary-to-decimal.html',
  styleUrls: ['./binary-to-decimal.less']
})
export class BinaryToDecimalComponent implements OnInit {
  public Binary: string = '101';
  public Steps: IConversionStep[] = [];

  public ngOnInit(): void {
    this.ConvertAllBase();
  }

  public ConvertAllBase() {
    this.Steps = this.Convert(this.Steps);
  }

  public IsValidBinary(): boolean {
    return /^[0-1]+$/.test(this.Binary);
  }

  public Convert(steps: IConversionStep[]): IConversionStep[] {
    steps = [];

    for (let i = this.Binary.length - 1, j = 0; i >= 0; i--) {
      const base = Math.pow(2, j++);
      const value = Number(this.Binary[i]);

      steps.push({ Value: value, Remainder: value * base, Base: base });
    }

    return steps;
  }

  public ToCalcul(steps: IConversionStep[]): IConversionStep[] {
    return steps.filter(step => step.Remainder > 0).reverse();
  }

  public ToResult(steps: IConversionStep[]): number {
    let result = 0;

    steps.forEach(step => result += step.Remainder);

    return result;
  }
}
