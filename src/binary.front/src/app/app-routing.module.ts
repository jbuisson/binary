import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DecimalToBinaryComponent } from './decimal-to-binary/decimal-to-binary';
import { BinaryReaderComponent } from './binary-reader/binary-reader.component';
import { BinaryToDecimalComponent } from './binary-to-decimal/binary-to-decimal';

const routes: Routes = [
  {
    path: 'converter',
    component: DecimalToBinaryComponent
  },
  {
    path: 'decimal-to-binary',
    component: DecimalToBinaryComponent
  },
  {
    path: 'binary-to-decimal',
    component: BinaryToDecimalComponent
  },
  {
    path: 'reader',
    component: BinaryReaderComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {

}
